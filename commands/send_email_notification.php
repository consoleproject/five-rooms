<?php
    require $_SERVER["DOCUMENT_ROOT"]."helpers/constants/email_functionality_constants.php";
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    function send_email_notification($email, $message, $message_simple) {
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = SMTP::DEBUG_SERVER; // Enable verbose debug output
            $mail->isSMTP(); // Send using SMTP
            $mail->Host       = MAIL_HOST; // Set the SMTP server to send through
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = MAIL_USERNAME; // SMTP username
            $mail->Password   = MAIL_PASSWORD; // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // Enable implicit TLS encryption
            $mail->Port       = 465; // TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $mail->setFrom(MAIL_SENDER, 'Five-rooms test project');
            $mail->addAddress($email); // recepient email address
            $mail->addReplyTo(MAIL_SENDER, 'Five-rooms test project');

            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'Successful reservation';
            $mail->Body    = $message;
            $mail->AltBody = $message_simple;
            $mail->send();
        } catch (Exception $e) {
            echo "\tMessage could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
?>