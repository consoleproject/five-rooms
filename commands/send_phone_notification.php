<?php
    require $_SERVER["DOCUMENT_ROOT"]."helpers/constants/sms_functionality_constants.php";

    use Twilio\Rest\Client;
    
    function send_phone_notification($number, $message) {
        $client = new Client(ACCOUNT_SID, AUTH_TOKEN);
        $client->messages->create(
            $number, // send to
            array(
                'from' => TWILIO_NUMBER, // send from
                'body' => $message // notification
            )
        );
    }
?>