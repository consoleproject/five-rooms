<?php 
    require $_SERVER["DOCUMENT_ROOT"]."db_props.php";
    require $_SERVER["DOCUMENT_ROOT"]."helpers/constants/formatting_constants.php";
    
    $connection = null;

    function connect_to_db() {
        $host = HOST;
        $port = PORT;
        $dbname = DBNAME;
        $dbuser = DBUSER;
        $dbpassword = DBPASSWORD;

        $conn = pg_connect( "host = $host port = $port dbname = $dbname user = $dbuser password = $dbpassword" );
        
        if (!$conn) {
            echo "\n\t".RED."Error: Unable to open database $dbname.".NORMAL."\n";
            echo "\tExiting...\n";
            die();
        } 
        echo "\n".GREEN."\tEstablished connection".NORMAL." to '".UNDERLINED."$dbname".NORMAL."' database\n";

        global $connection;
        $connection = $conn;

        $create_table = <<< EOF
            CREATE TABLE IF NOT EXISTS reservations (
                id SERIAL PRIMARY KEY,
                date DATE,
                room VARCHAR(1),
                reserved_from TIME,
                reserved_until TIME,
                reserved_by VARCHAR(30),
                reserver_email VARCHAR(30),
                reserver_number VARCHAR(15)
            )
        EOF;

        if (!pg_query($connection, $create_table)) {
            echo RED.pg_last_error($connection).NORMAL;
            echo "\tExiting...\n";
            die();
        } 
        echo "\tTable '".UNDERLINED."reservations".NORMAL."' is present in the database\n";

        $current_date = date("Y-m-d");
        $current_time = date("H:i:s");
        $delete_outdated_reservations = <<< EOF
            DELETE FROM reservations
            WHERE (date < '$current_date' :: date)
            OR (
                date = '$current_date' :: date
                AND
                reserved_until < '$current_time' :: time
            )
        EOF;

        if (!pg_query($connection, $delete_outdated_reservations)) {
            echo RED.pg_last_error($connection).NORMAL;
        } 
        echo "\n\tOutdated reservations cleared..\n\n";
    }

    function get_connection() {
        global $connection;
        return $connection;
    }
?>