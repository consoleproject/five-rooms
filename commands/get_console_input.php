<?php
    // formatting constants are available (should be available) in the calling scripts
    function get_console_input($message) {
        echo "\t".CYAN.$message.NORMAL;
        fscanf(STDIN, "%s", $buffer);
        return $buffer;
    }
?>