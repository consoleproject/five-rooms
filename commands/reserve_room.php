<?php
    require "get_reserver_credentials.php";
    require "send_email_notification.php";
    require "send_phone_notification.php";

    function reserve_room($conn, $room, $time, $date) {
        $reserved_from = $time[0];
        $reserved_until = $time[1];

        $get_rows_that_match_given_args = <<< EOF
            SELECT * FROM reservations 
            WHERE date = '$date' :: date AND room = '$room'
            AND ((
                    '$reserved_until' :: time < reserved_until
                    AND  
                    '$reserved_until' :: time > reserved_from
                ) 
                OR (
                    '$reserved_from' :: time < reserved_until 
                    AND 
                    '$reserved_from' :: time > reserved_from 
                )
                OR (
                    '$reserved_from' :: time = reserved_from
                    AND
                    '$reserved_until' :: time = reserved_until
                )
            )
        EOF;
        
        $matching_rows = pg_query($conn, $get_rows_that_match_given_args);
        if (!$matching_rows) {
            echo RED.pg_last_error($conn).NORMAL;
            echo "\tExiting...\n";
            die();
        } 
        echo "\tChecking database for concurrent reservations..\n\n";
        $matching_rows = pg_fetch_all($matching_rows);
        $number_of_matching_rows = count($matching_rows);
            
        if ($number_of_matching_rows == 0) {
            // insert new row as there are no reservations at given date, time and room
            echo "\t-------------------------------------------------\n";
            echo "\t------- Room is available for reservation -------\n";
            echo "\t-------------------------------------------------\n";
            echo "\n\tTime: '$reserved_from' - '$reserved_until'\n";
            echo "\tDate: '$date'\n";
            echo "\tRoom: '$room'\n\n";

            $reserver_credentials = get_reserver_credentials();
            $reserved_by = $reserver_credentials["f_name"]." ".$reserver_credentials["l_name"];
            $reserver_email = $reserver_credentials["email"];
            $reserver_number = "+998".$reserver_credentials["phone"];

            $insert_new_reservation = <<< EOF
                INSERT INTO reservations (date, room, reserved_from, reserved_until, reserved_by, reserver_email, reserver_number)
                VALUES(
                    '$date' :: date, 
                    '$room', 
                    '$reserved_from' :: time, 
                    '$reserved_until' :: time,
                    '$reserved_by',
                    '$reserver_email',
                    '$reserver_number'
                )
            EOF;
            $reservation_added = pg_query($conn, $insert_new_reservation);
            if (!$reservation_added) {
                echo RED.pg_last_error($conn).NORMAL;
                echo "\n\t".RED."Couldn't insert row into table".NORMAL."\n";
                echo "\tExiting...\n";
                die();
            } 
            echo "\n\t".GREEN."Reservation is saved..".NORMAL."\n\n";

            $notification_message_phone = "Hi $reserved_by,\n".
            "Your reservation was successful.\n".
            "You have reserved room# '$room' on '$date' from '$reserved_from' until '$reserved_until.'\n".
            "Have a great day!\n";

            $notification_message_email = "Hi $reserved_by,<br/>".
            "Your reservation was successful.<br/>".
            "You have reserved room# '$room' on '$date' from '$reserved_from' until '$reserved_until.'<br/>".
            "Have a great day!<br/>";
            
            send_email_notification($reserver_email, $notification_message_email, $notification_message_phone);
            send_phone_notification($reserver_number, $notification_message_phone);
        } else {
            $room_is_reserved_until = $matching_rows[$number_of_matching_rows - 1]['reserved_until'];
            $room_is_reserved_by = $matching_rows[$number_of_matching_rows - 1]['reserved_by'];
            echo "\t".CYAN."Room is reserved until ".UNDERLINED.$room_is_reserved_until.NORMAL."\n";
            echo "\t".CYAN."Reserver : ".UNDERLINED.$room_is_reserved_by.NORMAL."\n\n";
        }

    }
?>