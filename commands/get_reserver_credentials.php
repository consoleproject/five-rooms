<?php
    require $_SERVER["DOCUMENT_ROOT"]."get_console_input.php";
    require $_SERVER["DOCUMENT_ROOT"]."get_console_input_email.php";
    require $_SERVER["DOCUMENT_ROOT"]."phone_notification/get_console_input_phone.php";
    require $_SERVER["DOCUMENT_ROOT"]."helpers/constants/regex.php";
    require $_SERVER["DOCUMENT_ROOT"]."phone_notification/mobile_operators_codes.php";

    function get_reserver_credentials() {
        // get_console_input() - customized fscanf()
        $first_name = get_console_input("First name : ");
        $last_name = get_console_input("Last name  : ");
        $email = get_console_input_email("Email      : ");
        $phone = get_console_input_phone("Phone      : ");

        return array(
            "f_name" => $first_name,
            "l_name" => $last_name,
            "email" => $email,
            "phone" => $phone
        );
    }
?>