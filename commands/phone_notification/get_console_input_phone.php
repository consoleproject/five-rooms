<?php
    function get_console_input_phone($message) {
        $is_wrong_number = false;
        $is_wrong_operator_code = false;
        do {
            if($is_wrong_number) {
                echo "\n\t".RED."Error! --> Wrong number format".NORMAL."\n";
                echo "\t".YELLOW."Enter 9 digits without country code, eg 997138855".NORMAL."\n\n";
            }
            $phone = get_console_input($message);
            $is_wrong_number = !preg_match(PHONE_REGEX, $phone);
            $operator_code = substr($phone, 0, 2);
            if(!in_array($operator_code, BEELINE_CODES) &&
               !in_array($operator_code, HUMANS_CODES) &&
               !in_array($operator_code, PERFECTUM_CODES) &&
               !in_array($operator_code, UCELL_CODES) &&
               !in_array($operator_code, UMS_CODES) &&
               !in_array($operator_code, UZMOBILE_CODES)) {
                echo "\n\t".RED."Error! --> Found $operator_code. No such operator".NORMAL."\n";
                echo "\t".YELLOW."Available operator codes are: 33, 88, 90, 91, 93, 94, 95, 97, 98, 99".NORMAL."\n";
                echo "\tFor example 997138855 --> 99 713 88 55\n";
                $is_wrong_operator_code = true;
                $is_wrong_number = true;
            }
        } while ($is_wrong_number && $is_wrong_operator_code);

        return $phone;
    }
?>