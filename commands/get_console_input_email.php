<?php
    function get_console_input_email($message) {
        $is_wrong_email = false;
        do {
            if($is_wrong_email) {
                echo "\n\t".RED."Error! --> Wrong email format".NORMAL."\n";
            } 
            $email = get_console_input($message);
            $is_wrong_email = !preg_match(EMAIL_REGEX, $email);
        } while ($is_wrong_email);

        return $email;
    }
?>