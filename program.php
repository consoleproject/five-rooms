<?php 
// 'connect_to_db.php' is alreardy required in 'commands/reserve_room.php'
require $_SERVER["DOCUMENT_ROOT"]."commands/connect_to_db.php";
require $_SERVER["DOCUMENT_ROOT"]."commands/reserve_room.php";
require $_SERVER["DOCUMENT_ROOT"]."helpers/program_usage.php";
require $_SERVER["DOCUMENT_ROOT"]."helpers/input_args/check_args.php";

require 'vendor/autoload.php';

// clear screen
$os = php_uname('s');
if(strpos($os, "Linux") >= 0) {
    system("clear");
} elseif (strpos($os, "Win") >= 0) {
    system("cls");
} else {
    echo "\e[1;1H\e[J";
}

connect_to_db(); // from 'connect_to_db.php'

$connection = get_connection(); // from 'connect_to_db.php' 

$this_file_name = $_SERVER['SCRIPT_NAME'];
$passed_args = $argv;
array_shift($passed_args); // remove script name
$passed_args_count = count($passed_args);

switch ($passed_args_count) {
    case 2:
        // current date
        $checked_args = check_args($passed_args[0], $passed_args[1], date("Y-m-d"));

        if(!$checked_args) {
            die();
        }

        $room = $checked_args[0];
        $time = $checked_args[1]; // array of 2 elements
        $date = $checked_args[2];

        reserve_room($connection, $room, $time, $date);
        break;
    case 3:
        $checked_args = check_args($passed_args[0], $passed_args[1], $passed_args[2]);

        if(!$checked_args) {
            die();
        }

        $room = $checked_args[0];
        $time = $checked_args[1]; // array of 2 elements
        $date = $checked_args[2];
        

        
        reserve_room($connection, $room, $time, $date);
        break;
    default:
        display_program_usage($this_file_name);
        die();
}
?>