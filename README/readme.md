### 'Five-rooms' test project assigned by AlifTech
#### Author: Saidazimkhuja Saidnabiev


### Prerequsites
* php
* postgresql
* linux terminal

### How to run?

#### Step 1
Create database called 'aliftech'

#### Step 2
Edit `five-rooms/db_props.php` to specify YOUR database properties

![alt text](Screenshots/Screenshot from 2022-07-10 14-31-41.png)

#### Step 3 (If you want email notification functionality)
Edit `five-rooms/helpers/constants/email_functionality_constants.php` to specify YOUR mail host and account.
**Those already specified are no more valid!**.

![alt text](Screenshots/Screenshot from 2022-07-10 14-32-34.png)

#### Step 4 (If you want phone notification functionality)
Edit `five-rooms/helpers/constants/sms_functionality_constants.php` to specify YOUR Twilio sid, key and number **if the specified props are no more valid**. 

![alt text](Screenshots/Screenshot from 2022-07-10 14-31-05.png)

#### Step 5 - Run `php program.php arg1 arg2 [arg3]`
Usage of 'program.php':
* 	  `program.php arg1 arg2` (reserve **today**)
* 	  `program.php arg1 arg2 arg3`
 	  <br>&nbsp;`arg1` - room number (eg 1/2/3/4/5)
 	  <br>&nbsp;`arg2` - reservation time (eg 13:45:00-16:00:00, ie 13:45:00>16:00:00)
 	  <br>&nbsp;`arg3` - reservation date (eg 2022-08-01, maximum for 5 month ahead)
	
![alt text](Screenshots/Screenshot from 2022-07-10 15-18-25.png)

### Screenshots 
#### Run the program

![alt text](Screenshots/Screenshot from 2022-07-10 14-48-16.png)
#### Room is available, so make reservation

![alt text](Screenshots/Screenshot from 2022-07-10 14-48-55.png)
#### All fields are correct, proceed..

![alt text](Screenshots/Screenshot from 2022-07-10 14-49-25.png)
#### Received sms notification

![alt text](Screenshots/photo_2022-07-10_14-50-49.jpg)
#### Received email notification

![alt text](Screenshots/Screenshot from 2022-07-10 14-50-20.png)
#### Sms sent on messaging log of Twilio platform (free trial)

![alt text](Screenshots/Screenshot from 2022-07-10 14-53-56.png)
#### Display name of the reserver and until time if there is concurrent reservation

![alt text](Screenshots/Screenshot from 2022-07-10 14-33-34.png)
### Additional projects used

* [phpMailer](https://github.com/PHPMailer/PHPMailer) - email notification
* [twilio](https://www.twilio.com/) - sms notifications
<br><br>All provided screenshots belong to the author.
