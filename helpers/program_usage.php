<?php

    function display_program_usage($script_name) {
        echo "\n\t".YELLOW."Warning!!!".NORMAL."\n";
        echo "\tUsage of '$script_name':\n";
        echo "\t  ".CYAN."$script_name arg1 arg2".NORMAL." (".YELLOW."reserve today".NORMAL.")\n";
        echo "\t  ".CYAN."$script_name arg1 arg2 arg3".NORMAL."\n\n";
        echo "\targ1 - room number (eg 1/2/3/4/5)\n";
        echo "\targ2 - reservation time (eg 13:45:00-16:00:00, ie 13:45:00>16:00:00)\n";
        echo "\targ3 - reservation date (eg 2022-08-01, maximum for 5 month ahead)\n\n";
    }
    
?>