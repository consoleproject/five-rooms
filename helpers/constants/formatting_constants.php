<?php
    define("BOLD", "\e[1m");
    define("UNDERLINED", "\e[4m");
    define("NORMAL", "\e[0m");

    // colors
    define("YELLOW", "\e[33m");
    define("RED", "\e[91m");
    define("GREEN", "\e[92m");
    define("CYAN", "\e[96m");
?>