<?php
    function check_date($date) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            $date = explode('-', $date); // to check
            $today = date("Y-m-d");
            $curr_date = explode('-', $today);
            $year = intval($date[0]); 
            $month = intval($date[1]); 
            $day = intval($date[2]);

            $curr_year = intval($curr_date[0]); 
            $curr_month = intval($curr_date[1]); 
            $curr_day = intval($curr_date[2]);
            $ahead_time = null;

            // types of errors
            $too_future_date = false;
            $past_date = false;
            $month_ahead = false;

            if  ($year - $curr_year > 1) {
                $too_future_date = true;
            } elseif ($year - $curr_year < 0) {
                $past_date = true;
            } elseif ($year - $curr_year == 1 && ($month + (12 - $curr_month) > 5 || $month + (12 - $curr_month) >= 5 && $day > $curr_day)) {
                // month are assumed to have the same number of days
                $ahead_time = $month + (12 - $curr_month);
                $month_ahead = true;
            } elseif ($year == $curr_year && ($month - $curr_month > 5 || $month - $curr_month >= 5 && $day > $curr_day)) {
                // month are assumed to have the same number of days
                $month_ahead = true;
            } elseif ($year == $curr_year && $month - $curr_month < 0) {
                $past_date = true;
            }

            if ($too_future_date || $past_date || $month_ahead) {
                echo "\n\t".RED."Error! --> Date Format".NORMAL."\n";
                $month_ahead ? print_r("\tProvided date is more than $ahead_time month ahead.\n") : null;
                $too_future_date || $month_ahead ? print_r("\t".YELLOW."You can reserve for maximum 5 month.\n") : null;
                $past_date ? print_r("\tDate cannot be less than today, today is '$today'.\n") : null;
                echo "\n\tExiting...\n\n";
                return false;
            }
        } else {
            echo "\n\t".RED."Error! --> Date Format".NORMAL."\n";
            echo "\tFound '$date'. Impoper date format.\n";
            echo "\n\tExiting...\n\n";
            return false;
        }

        // passed the test
        echo "\t".GREEN."The date format".NORMAL." is ok..\n\n";

        return implode('-', $date);
    }
?>