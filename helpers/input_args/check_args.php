<?php
    require "check_time_format.php";
    require "check_date_format.php";

    function check_args($room, $time, $date) {
        if ($room < 1 || $room > 5) {
            echo "\n\t".RED."Error!\n";
            echo "\tRoom does not exist!".NORMAL."\n";
            echo "\tPlease insert integer from 1 to 5\n";
            echo "\tExiting...\n";
            display_program_usage($_SERVER['SCRIPT_NAME']);
            return false;
        }

        echo "\t".GREEN."The room exists".NORMAL."\n";

        $time = check_time($time);
        $date = check_date($date);

        if (!$time || !$date) {
            echo "\n\t".YELLOW."Warning!".NORMAL."\n";
            echo "\tPlease read program usage carefully\n";
            display_program_usage($_SERVER['SCRIPT_NAME']);
            return false;
        }

        $response = array($room, $time, $date);

        return $response;

    }
?>