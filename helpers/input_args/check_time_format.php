<?php
    function check_time($time) {
        $time_interval = explode('-', $time);
        
        foreach ($time_interval as $key => $value) {
            if (!preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/", $value)) {
                echo "\n\t".RED."Error --> Time Format".NORMAL."\n";
                echo "\tFound '$value'. Improper time format.\n";
                return false;
            }
        }

        if(strtotime($time_interval[0]) > strtotime($time_interval[1])) {
            echo "\n\t".RED."Error! --> Time Format".NORMAL."\n";
            echo "\tStart time should be less than end time\n";
            echo "\tFound ".$time_interval[0]." > ".$time_interval[1]."\n\n";
            return false;
        }

        // passed the test
        echo "\t".GREEN."The time format".NORMAL." is ok..\n";

        return $time_interval;
    }
?>